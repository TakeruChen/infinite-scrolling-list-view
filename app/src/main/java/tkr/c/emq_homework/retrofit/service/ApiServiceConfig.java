package tkr.c.emq_homework.retrofit.service;

import com.google.gson.Gson;

/**
 * Created by ixtli on 2014/7/10.
 */
public class ApiServiceConfig {

	public static final Gson sGson = new Gson();
	
    public static final String HTTP = "http://";
    public static final String HTTPS = "https://";
    public static final String ROOT_PATH = "/";
    public static final String MIME_JSON = "application/json";

    //Production server
    public static final String WEBSERVICE_SERVER_URL = ApiServerConfig.WEBSERVICE_SERVER_URL;
    public static final int WEBSERVICE_TIMEOUT_MINUTE = 30;
}
