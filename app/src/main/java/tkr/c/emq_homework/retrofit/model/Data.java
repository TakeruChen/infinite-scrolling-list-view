package tkr.c.emq_homework.retrofit.model;

/**
 * Created by ixtli on 2016/1/13.
 */
public class Data {
    private String created;
    private int id;
    private source source;
    private destination destination;

    public String getCreated() {
        return created;
    }

    public int getId() {
        return id;
    }

    public Data.source getSource() {
        return source;
    }

    public Data.destination getDestination() {
        return destination;
    }

    public class source{
        String sender;
        String note;

        public String getSender() {
            return sender;
        }

        public String getNote() {
            return note;
        }
    }

    public class destination{
        String recipient;
        String amount;
        String currency;

        public String getRecipient() {
            return recipient;
        }

        public String getAmount() {
            return amount;
        }

        public String getCurrency() {
            return currency;
        }
    }
}
