package tkr.c.emq_homework.retrofit.service;

import com.squareup.okhttp.OkHttpClient;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import retrofit.RestAdapter;
import retrofit.client.OkClient;


public class ApiClient {

        private static final String BASE_URL = ApiServiceConfig.WEBSERVICE_SERVER_URL;
        private RestService apiService;

        public ApiClient(){
            try {
                final TrustManager[] trustAllCerts = new TrustManager[] {
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(
                                X509Certificate[] chain,
                                String authType) throws CertificateException {
                        }

                        @Override
                        public void checkServerTrusted(
                                X509Certificate[] chain,
                                String authType) throws CertificateException {
                        }
                    
                        @Override
                        public X509Certificate[] getAcceptedIssuers() {
                            return null;
                        }
                    }
                };

                // Install the all-trusting trust manager
                final SSLContext sslContext = SSLContext.getInstance("SSL");
                sslContext.init(null, trustAllCerts, new java.security.SecureRandom());

                // Create an ssl socket factory with our all-trusting manager
                final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

                //Use OKHttpClient
                OkHttpClient okHttpClient = new OkHttpClient();
                okHttpClient.setReadTimeout(ApiServiceConfig.WEBSERVICE_TIMEOUT_MINUTE, TimeUnit.SECONDS);
                okHttpClient.setConnectTimeout(ApiServiceConfig.WEBSERVICE_TIMEOUT_MINUTE, TimeUnit.SECONDS);
                okHttpClient.setSslSocketFactory(sslSocketFactory);
                okHttpClient.setHostnameVerifier(new HostnameVerifier() {
                     @Override
                     public boolean verify(String hostname, SSLSession session) {
                         return true;
                     }
                });

                RestAdapter restAdapter = new RestAdapter.Builder()
                    .setLogLevel(RestAdapter.LogLevel.FULL)
                    .setEndpoint(BASE_URL)
//                        .setClient(new OkClient(okHttpClient))
                    .build();

                apiService = restAdapter.create(RestService.class);
            }
            catch (Exception e) {
                    throw new RuntimeException(e);
            }
        }

        public RestService getApiService(){
            return apiService;
        }
}
