package tkr.c.emq_homework.retrofit.service;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;
import tkr.c.emq_homework.retrofit.model.Data;

public interface RestService {

    //GetList
    @GET("/infinite-list")
    void getList(@Query("startIndex") int startIndex,
                 @Query("num") int num,
                 Callback<ArrayList<Data>> cb);
}