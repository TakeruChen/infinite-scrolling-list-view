package tkr.c.emq_homework.retrofit;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import tkr.c.emq_homework.MainActivity;
import tkr.c.emq_homework.R;
import tkr.c.emq_homework.retrofit.model.Data;


/**
 * Created by ixtli on 2016/1/14.
 */
public class DataListRecyclerAdapter extends RecyclerView.Adapter<DataListRecyclerAdapter.ListItemViewHolder>{

    private static final String TAG = DataListRecyclerAdapter.class.getSimpleName();

    private int mMaxArraySize;
    private int mDataFetchCount;
    private ArrayList<Data> mItems;
    private OnItemClickListener mOnItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(View view, Data item);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener){
        mOnItemClickListener = onItemClickListener;
    }

    public DataListRecyclerAdapter(int maxArraySize, int dataFetchCount){
        this.mItems = new ArrayList<>();
        this.mMaxArraySize = maxArraySize;
        this.mDataFetchCount = dataFetchCount;
    }

    public void setData(ArrayList<Data> items){
        this.mItems = items;
        notifyDataSetChanged();
    }

    public void addDataToBottom(ArrayList<Data> items){
        mItems.addAll(mItems.size(), items);
        notifyItemRangeInserted(mItems.size(), items.size());
//        for(int i=0;i<items.size();i++){
//            mItems.add(items.get(i));
//            notifyItemInserted(mItems.size()-1);
//        }
    }

    public void addDataToTop(ArrayList<Data> items){
        mItems.addAll(0, items);
        notifyItemRangeInserted(0, items.size());
//        for(int i=0;i<items.size();i++){
//            mItems.add(items.get(i));
//            notifyItemInserted(mItems.size()-1);
//        }
    }

    public void deleteTopData(){
        for(int i=0 ; i<mDataFetchCount ; i++) {
            mItems.remove(0);
            notifyItemRemoved(0);
        }
    }

    public void deleteBottomData(){
        for(int i=0 ; i<mDataFetchCount ; i++) {
            mItems.remove(mItems.size()-1);
            notifyItemRemoved(mItems.size()-1);
        }
    }

    public int getDataId(int position){
        if (position < mItems.size() && position >= 0)
            return mItems.get(position).getId();
        else
            return -1;
    }

    public int getFirstDataId(){
        return mItems.get(0).getId();
    }

    public int getLastDataId(){
        return mItems.get(mItems.size()-1).getId();
    }

    @Override
    public ListItemViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.listitem_data, viewGroup, false);
        return new ListItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ListItemViewHolder viewHolder, int position) {
        Data data = mItems.get(position);
        viewHolder.itemView.setTag(data);

        viewHolder.dataId.setText("Id: " + data.getId());
        viewHolder.dataCreated.setText(data.getCreated());

        viewHolder.dataSender.setText("Sender: " + data.getSource().getSender());
        viewHolder.dataNote.setText("Note: " + data.getSource().getNote());

        viewHolder.dataAmount.setText(data.getDestination().getAmount());
        viewHolder.dataCurrency.setText(data.getDestination().getCurrency());
        viewHolder.dataRecipient.setText(data.getDestination().getRecipient());
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }


    public final class ListItemViewHolder extends RecyclerView.ViewHolder {
        public TextView dataId;
        public TextView dataCreated;
        public TextView dataSender;
        public TextView dataNote;
        public TextView dataAmount;
        public TextView dataCurrency;
        public TextView dataRecipient;

        public ListItemViewHolder(final View itemView) {
            super(itemView);
            dataId = (TextView) itemView.findViewById(R.id.listitem_data_id);
            dataCreated = (TextView) itemView.findViewById(R.id.listitem_data_created);
            dataSender = (TextView) itemView.findViewById(R.id.listitem_data_sender);
            dataNote = (TextView) itemView.findViewById(R.id.listitem_data_note);
            dataAmount = (TextView) itemView.findViewById(R.id.listitem_data_amount);
            dataCurrency = (TextView) itemView.findViewById(R.id.listitem_data_currency);
            dataRecipient = (TextView) itemView.findViewById(R.id.listitem_data_recipient);
        }
    }


}
