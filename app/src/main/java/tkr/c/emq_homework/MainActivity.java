package tkr.c.emq_homework;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import tkr.c.emq_homework.retrofit.DataListRecyclerAdapter;
import tkr.c.emq_homework.retrofit.model.Data;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    private static final int DATA_FETCH_PREV = 1;
    private static final int DATA_FETCH_NEXT = 2;
    private static final int DATA_FETCH_COUNT = 30;
    private static final int DATA_MAX_ARRAY_SIZE = DATA_FETCH_COUNT * 3 ;

    private Toolbar mToolbar;
    private ProgressBar mProgressBar;

    private RecyclerView mRecycleView;
    private LinearLayoutManager mLinearLayoutManager;
    private DataListRecyclerAdapter mDataListAdapter;

    private boolean isLoading = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById();
        setToolbar();

        //initial ListView
        setListView();

        //first time get data from API
        getDataList(1, DATA_FETCH_NEXT);

        //for API testing
        (findViewById(R.id.main_btn_getlist)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDataList(1, DATA_FETCH_NEXT);
            }
        });
    }

    private void findViewById(){
        mToolbar = (Toolbar) findViewById(R.id.main_toolbar);
        mRecycleView = (RecyclerView) findViewById(R.id.main_listview);
        mProgressBar = (ProgressBar) findViewById(R.id.main_progressbar);
    }

    private void setToolbar(){
        if (mToolbar != null) {
            setSupportActionBar(mToolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            mToolbar.setNavigationIcon(R.mipmap.ic_menu);
            mToolbar.setTitle(R.string.title);
        }
    }

    private void setListView(){
        mDataListAdapter = new DataListRecyclerAdapter(DATA_MAX_ARRAY_SIZE, DATA_FETCH_COUNT);
        mLinearLayoutManager = new LinearLayoutManager(this);
        mRecycleView.setLayoutManager(mLinearLayoutManager);
        mRecycleView.setAdapter(mDataListAdapter);
        mRecycleView.addOnScrollListener(onScrollListener);
    }

    private void getDataList(int firstItemIndex, final int fetchType){
        AppApplication.getApiService().getList(firstItemIndex, DATA_FETCH_COUNT, new Callback<ArrayList<Data>>() {
            @Override
            public void success(ArrayList<Data> datas, Response response) {
                Log.d(TAG, "success");
//                Log.d(TAG, new Gson().toJson(datas));

                if (fetchType == DATA_FETCH_NEXT) {
                    int itemCount = mLinearLayoutManager.getItemCount();
                    if (itemCount == DATA_MAX_ARRAY_SIZE) {
                        //delete data
                        mDataListAdapter.deleteTopData();
                    }

                    if (mLinearLayoutManager.getItemCount() == 0) {
                        mDataListAdapter.setData(datas);
                        mProgressBar.setVisibility(View.GONE);
                    } else {
                        mDataListAdapter.addDataToBottom(datas);
                    }
                }
                else if(fetchType == DATA_FETCH_PREV){
                    int itemCount = mLinearLayoutManager.getItemCount();
                    if (itemCount == DATA_MAX_ARRAY_SIZE) {
                        //delete bottom data
                        mDataListAdapter.deleteBottomData();
                        Log.d(TAG, "delete bottom data: ");
                        Log.d(TAG, "itemCount: " + mLinearLayoutManager.getItemCount());
                    }

                    mDataListAdapter.addDataToTop(datas);
                    Log.d(TAG, "itemCount: " + mLinearLayoutManager.getItemCount());
                }

                isLoading = false;
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(TAG, "failure");
                Log.d(TAG, error.getMessage());
                isLoading = false;
            }
        });
    }

    RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
//            Log.d(TAG, "onScrolled " + dx + " " + dy);

            //totalItems
            int itemCount = mLinearLayoutManager.getItemCount();
            //visibleItems
            int childCount = mRecycleView.getChildCount();
            int firstVisibleItemPosition = mLinearLayoutManager.findFirstVisibleItemPosition();
            int lastVisibleItemPosition = mLinearLayoutManager.findLastVisibleItemPosition();
//            Log.d(TAG, "childCount: " + childCount);
            Log.d(TAG, "itemCount: " + itemCount);
//            Log.d(TAG, "firstVisibleItemPosition: " + firstVisibleItemPosition);
//            Log.d(TAG, "lastVisibleItemPosition: " + lastVisibleItemPosition);


            if (dy > 0) {
                // scrolling up
//                Log.d(TAG, "onScrolled up" + dx + " " + dy);

                //when user scroll to end of listView
                int lastVisibleItemId = mDataListAdapter.getDataId(lastVisibleItemPosition+14);
                if (lastVisibleItemId == mDataListAdapter.getLastDataId() && isLoading == false){
                    isLoading = true;
                    Log.d(TAG, "lastVisibleItemId: " + lastVisibleItemId);
                    getDataList(lastVisibleItemId+1, DATA_FETCH_NEXT);
                }
            }
            else {
                // scrolling down
//                Log.d(TAG, "onScrolled down" + dx + " " + dy);

                //when user scroll to top of listView
                int firstVisibleItemId = mDataListAdapter.getDataId(firstVisibleItemPosition-10);
                if (firstVisibleItemId == mDataListAdapter.getFirstDataId() && isLoading == false){
                    if (firstVisibleItemId > DATA_FETCH_COUNT) {
                        isLoading = true;
                        Log.d(TAG, "firstVisibleItemId: " + firstVisibleItemId);
                        getDataList(firstVisibleItemId - DATA_FETCH_COUNT, DATA_FETCH_PREV);
                    }
                }
            }
        }
    };
}
