package tkr.c.emq_homework;

import android.app.Application;
import android.content.Context;
import android.content.res.Resources;

import tkr.c.emq_homework.retrofit.service.ApiClient;
import tkr.c.emq_homework.retrofit.service.RestService;


public class AppApplication extends Application{

    private static final String TAG = AppApplication.class.getSimpleName();
    private static AppApplication sInstance;
    private static RestService mRestService;
    private static ApiClient sApiClient;
    

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;

        //initial REST ApiClient
        sApiClient = new ApiClient();
    }

    /**
     * @return the main context of the Application
     */
    public static Context getAppContext() {
        return sInstance;
    }

    /**
     * @return the main resources from the Application
     */
    public static Resources getAppResources() {
        return sInstance.getResources();
    }

    public static ApiClient getRestClient(){
        return sApiClient;
    }

    public static RestService getApiService(){
        if (sApiClient == null)
            sApiClient = new ApiClient();
        return sApiClient.getApiService();
    }
}
